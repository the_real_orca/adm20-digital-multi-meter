BSIDE ADM20 DMM Protocol
========================

[TOC]

The ADM20 Digital Multi Meter uses a serial protocol to send measurements to the PC.
It looks like that the serial data are linked directly to the display.
There is no back channel.

- Parameter: **2400 baud 8N1**
- Packet Length: **22 bytes**

data packet description:

|Byte:|  1 - 6          |  7 - 10       |  11         |  12 - 18                |  19 - 22         |
|----:|:---------------:|:-------------:|:-----------:|:-----------------------:|:----------------:|
|     |[Header](#markdown-header-header)|[Value](#markdown-header-value)|[Mode](#markdown-header-mode)|[Analog Bar](#markdown-header-analog-bar)|[Mode](#markdown-header-mode-unit)|


## Header
*Byte: 1-6*

    AA 55 52 24 01 10

## Value
*Byte: 7-10*

Each byte represents one digit of the 7-segment display. They are send in reverse order.

- **Bit 1-7**: 7-segment display
- **Bit 8**: decimal point

| Segment to Bit mapping: |  |  |
|-:|:-----:|:-|
|  |== 1 ==|  |
|II|       |II|
|5 |       |2 |
|II|       |II|
|  |== 6 ==|  |
|II|       |II|
|7 |       |3 |
|II|       |II|
|  |== 4 ==|  |
    

The 7-segment display coding leads to the following translation table:

|Value (hex)|decoded|
|:---------:|:-----:|
| 00 | *empty* |
| 5F | 0 |
| 06 | 1 |
| 6B | 2 |
| 2F | 3 |
| 36 | 4 |
| 3D | 5 |
| 7D | 6 |
| 07 | 7 |
| 7F | 8 |
| 3D | 9 |
| 79 | E |
| 58 | L |

> This section is heavily based on the work of Andreas Reischle: [Arduino decodes the BSIDE ADM20 serial infrared protocol](http://www.areresearch.net/2017/04/arduinod17-project-arduino-decodes.html).

## Mode
*Byte: 11*

|Bit:| 8 | 7  |  6   | 5 | 4  | 3 | 2 |  1  |
|----|---|----|------|---|----|---|---|-----|
|    |   |beep|analog|   |sign|DC |AC |DIODE|

## Analog Bar
*Byte: 12-18*

Each bit represents one tick of the analog bar. The bar is encoded in MSB but needs to be drawn from left to right.

## Mode, Unit
*Byte: 19-22*

|Byte \\ Bit:| 8 | 7 | 6  | 5 | 4 | 3 | 2 | 1 |
|:----------:|---|---|----|---|---|---|---|---|
| **19**     |REL|   |AUTO|   |   |   |   |   |
| **20**     |   |hFE| %  |   |MIN|max-min|MAX|USB?|
| **21**     | F | n | u  |   |   |   |°F |°C |
| **22**     | Hz|Ohm| k  | M | V | A | m | u |