BSIDE ADM20 WiFi Extension
==========================

![ADM20_front.jpg](https://bytebucket.org/the_real_orca/adm20-digital-multi-meter/raw/e4275172fc042f6eab6243f6bdc768d3003016fd/images/overview.jpg)

This project replaces the USB interface with an ESP8266 based WiFi Web interface.

# Features

- **WiFi Digital Multimeter**
- **displays current mode, value and unit**
- **data history graph available**
- **works in stand-alone mode, or conneced to an existing network**
- **compatible with any modern browser**
- **no client SW required**
- **JSON data API**
- **reduce power consumption with deep sleep mode**
- **auto wake-up on data output**

# Documentation

See the Wiki for more information:

- [**Wiki Home**](https://bitbucket.org/the_real_orca/adm20-digital-multi-meter/wiki/)
- [**ADM20 Protocol Description**](https://bitbucket.org/the_real_orca/adm20-digital-multi-meter/wiki/Protocol)
- [**Power Reduction / Deep Sleep**](https://bitbucket.org/the_real_orca/adm20-digital-multi-meter/wiki/Deep%20Sleep%20Mode)


# Setup

## Hardware

- ESP8266 module (e.g. NodeMCU, WEMOS D1 mini, etc.)
- Connect one pin of the ESP8266 (default: D7) to the (inverted) raw Tx signal at the PCB between Q2 and R25, and GND to GND(-). If you want to power the ESP8266 from the ADM20 batteries, you can use the 3.3V power supply from Jumper J1 or J2 on the [ADM20 PCB](https://bytebucket.org/the_real_orca/adm20-digital-multi-meter/raw/34a3fd55fbc0d1a5ed39b6a63264d0d4d9e7df4e/images/ADM20_PCB.jpg).

> Do not power the ESP8266 from both, the USB and the ADM20 batteries at the same time!
>
> (Unless you have applied the [modifications](WEMOS%20D1%20mini%20Modification) to the WEMOS D1 mini PCB.)


## Required Libraries

- [WiFi Manager](https://github.com/the-real-orca/WiFiManager)
- [ArduinoJson](https://github.com/bblanchon/ArduinoJson)
- [WebSocketsServer](https://github.com/Links2004/arduinoWebSockets)


## Upload
- upload the compiled sketch
- upload the SPIFFS image (`./data` directory)

> **Attention: **
>
> **!!! Make sure that you DISCONNECT the USB before using the multimeter after programming !!!**


# Usage

- when started, the ESP8266 creates a new WiFi network: `adm20`
- connect to this WiFi network
- your browser should open with the ADM20 overview page (if not, go to `http://192.168.4.1`)
- don't forget to **activate USB** on the ADM20
