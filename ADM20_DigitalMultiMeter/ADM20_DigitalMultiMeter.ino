/**
* ADM20 ESP8266 Adapter
* https://bitbucket.org/the_real_orca/adm20-digital-multi-meter
*
* Copyright 2017 Stephan Veigl
*
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
* files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
* modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
* is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
* OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
* IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


// Settings

/// WiFi SSID if used in stand alone more, or setup SSID if used with WiFi Manager
#define AP_SSID	"ADM20"

/// move web root to sub directory to protect config files from access via http
#define WEB_ROOT "/www"

/// The ESP8266 Rx pin the ADM20 signal is connected to.
/// (The inverted signal is between Q2 and R25 on the ADM20 PCB. https://bitbucket.org/the_real_orca/adm20-digital-multi-meter/wiki/Home)
#define Rx_PIN	D7

/// Enable WiFi Manager to connect to existing network.
/// When disablet, the ESP8266 will set up its own AP and work in stand alone mode (@see AP_SSID).
#define WIFIMANAGER_ENABLED

/// WARNING: Requires extra HW! Check the link before enabling deep sleep.
/// Reduce power consumption and send ESP8266 to deep sleep mode, when not in use.
/// https://bitbucket.org/the_real_orca/adm20-digital-multi-meter/wiki/Deep%20Sleep%20Mode
//#define DEEPSLEEP_ENABLED

/// Enable Over the Air updates
#define OTA_UPDATE_ENABLED

/// Enable UPnP/SSDP device discovery
#define UPNP_ENABLED

/// enable debugging messages on serial output
#define DEBUG


// debug
#define LED 2
    #ifdef DEBUG
        #ifndef DEBUG_SERIAL
            #define DEBUG_SERIAL Serial
        #endif
        #define DEBUG_println(x) {DEBUG_SERIAL.print(millis()); DEBUG_SERIAL.print('~'); DEBUG_SERIAL.println(x);}
    #else
        #define DEBUG_println(x)
#endif


// WiFi
#include <ESP8266WiFi.h>
#include <WiFiClient.h>

// AMD20
#include "ADM20.h"
SoftwareSerial softSerial(Rx_PIN, 2, true); // RX, TX, inverted
ADM20 adm(softSerial);

#ifdef WIFIMANAGER_ENABLED
    // WiFi Manager
    // https://github.com/the-real-orca/WiFiManager
    #include <FS.h>
    #include <WiFiManager.h>
    //https://github.com/bblanchon/ArduinoJson
    #include <ArduinoJson.h>
    WiFiManager wifiManager;


    void load(WiFiManager &wifiManager) {
        //clean FS, for testing
        //  SPIFFS.format();

          //read configuration from FS json
        if (SPIFFS.exists("/config.json")) {
          //file exists, reading and loading
          File configFile = SPIFFS.open("/config.json", "r");
          if (configFile) {
            DEBUG_println("opened config file...");
            size_t size = configFile.size();
            // Allocate a buffer to store contents of the file.
            std::unique_ptr<char[]> buf(new char[size]);

            configFile.readBytes(buf.get(), size);
            DynamicJsonBuffer jsonBuffer;
            JsonObject& json = jsonBuffer.parseObject(buf.get());
            if (json.success()) {
              DEBUG_println("parsed json");
              if ( json["Networks"].is<JsonArray>() ) {
                for (int i=0; i < json["Networks"].size(); i++) {
                  auto obj = json["Networks"][i];

                  // add existing networks and credentials
                  wifiManager.addAP(obj["SSID"], obj["Password"]);
                }
              }
            } else {
              DEBUG_println("failed to load json config");
              return;
            }
          }
        }
    }

    void save(WiFiManager &wifiManager) {
      //save the custom parameters to FS
        DEBUG_println("saving config...");
        DynamicJsonBuffer jsonBuffer;
        JsonObject& json = jsonBuffer.createObject();
        JsonArray& networks = json.createNestedArray("Networks");

        // encode known networks to JSON
        for (int i = 0; auto ap = wifiManager.getAP(i); i++ ) {
          JsonObject& obj = jsonBuffer.createObject();
          obj["SSID"] = ap->ssid;
          obj["Password"] = ap->pass;
          networks.add(obj);
        }

        File configFile = SPIFFS.open("/config.json", "w");
        if (!configFile) {
            DEBUG_println("failed to open config file for writing");
            return;
        }
        json.printTo(configFile);
        configFile.close();
    }

    //flag for saving data
    bool shouldSaveConfig = false;
    //callback notifying us of the need to save config
    void saveConfigCallback () {
      shouldSaveConfig = true;
    }

#endif

// http httpServer
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <FS.h>
ESP8266WebServer httpServer(80);
File fsUploadFile;

// UPnP
#ifdef UPNP_ENABLED
    #include "SSDPDevice.h"
#endif

// web sockets
// https://github.com/Links2004/arduinoWebSockets
#include <Hash.h>
#include <WebSocketsServer.h>
WebSocketsServer webSocket = WebSocketsServer(8080);

// OTA Update
#ifdef OTA_UPDATE_ENABLED
    #include <ESP8266HTTPUpdateServer.h>
    ESP8266HTTPUpdateServer httpUpdater;
    const char* uploadForm = "<form method='POST' action='/upload' enctype='multipart/form-data'><input type='file' name='update'><input type='submit' value='Upload'></form>";
#endif


void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {
    switch(type) {
        case WStype_DISCONNECTED:
        break;
        case WStype_CONNECTED:
        break;
        case WStype_TEXT:
        break;
    }
}

#define JSON_OBJ_START	"{"
#define JSON_OBJ_END	"}"
#define JSON_NUMBER(name, val)  	("\"" + String(name) + "\":"+ String(val, 3))
#define JSON_STR(name, str)			("\"" + String(name) + "\":\""+ str + "\"")

String toDataJson() {
    String json = JSON_OBJ_START;
    json += JSON_STR("mode", adm.mode) + ",";
    json += JSON_STR("settings", adm.settings) + ",";
    if ( isnan(adm.value) )
        json += JSON_STR("value", "OL") + ",";
    else
        json += JSON_NUMBER("value", adm.value) + ",";
    json += JSON_STR("unit", adm.unit);
    json += JSON_OBJ_END;
    return json;
}

String getContentType(String filename){
    if(filename.endsWith(".htm")) return "text/html";
    else if(filename.endsWith(".html")) return "text/html";
    else if(filename.endsWith(".css")) return "text/css";
    else if(filename.endsWith(".js")) return "application/javascript";
    else if(filename.endsWith(".png")) return "image/png";
    else if(filename.endsWith(".gif")) return "image/gif";
    else if(filename.endsWith(".jpg")) return "image/jpeg";
    else if(filename.endsWith(".ico")) return "image/x-icon";
    else if(filename.endsWith(".xml")) return "text/xml";
    else if(filename.endsWith(".json")) return "text/json";
    else if(filename.endsWith(".pdf")) return "application/x-pdf";
    else if(filename.endsWith(".zip")) return "application/x-zip";
    else if(filename.endsWith(".gz")) return "application/x-gzip";
    return "text/plain";
}

bool handleFileRead(String path) {
    DEBUG_println("handleFileRead: " + path);
    String contentType = getContentType(path);
    // move web root
    path = WEB_ROOT + path;
    String pathWithGz = path + ".gz";
    bool gZip = false;

    if ( path.indexOf("..") >= 0 ) {
        // security alert -> trying to exit web root
        return false;
    }

    if ( SPIFFS.exists(pathWithGz) )	{
        DEBUG_println("use GZIP");
        gZip = true;
        path = pathWithGz;
    }
    if ( gZip || SPIFFS.exists(path) ) {
        File file = SPIFFS.open(path, "r");

        if ( gZip && contentType != "application/x-gzip" && contentType != "application/octet-stream" ) {
            httpServer.sendHeader("Content-Encoding", "gzip");
        }
        httpServer.setContentLength(file.size());
        httpServer.send(200, contentType, "");
        WiFiClient client = httpServer.client();
        client.setNoDelay(true);
        client.write(file, 2048);
        file.close();
        DEBUG_println("handleFileRead sent");
        return true;
    }
    return false;
}

void handleFileUpload() {
    HTTPUpload& upload = httpServer.upload();
    if ( upload.status == UPLOAD_FILE_START ) {
        String filename = upload.filename;
        if ( !filename.startsWith("/") )
        filename = "/"+filename;
        DEBUG_println("handleFileUpload Name:" + filename);
        // move web root
        filename = WEB_ROOT + filename;
        fsUploadFile = SPIFFS.open(filename, "w");
        filename = String();
    } else if( upload.status == UPLOAD_FILE_WRITE ) {
        if ( fsUploadFile )
        fsUploadFile.write(upload.buf, upload.currentSize);
    } else if( upload.status == UPLOAD_FILE_END ) {
        if ( fsUploadFile )
        fsUploadFile.close();
        DEBUG_println("handleFileUpload Size:" + String(upload.totalSize));
    }
}

void setup() {

    pinMode(LED, OUTPUT);
    digitalWrite(LED, 0);

    #ifdef DEBUG
        DEBUG_SERIAL.begin(115200);
        while(!DEBUG_SERIAL) {}
        delay(200);
        DEBUG_println("\n\n");
        DEBUG_println("starting ...");
    #endif

    // init ADM20
    adm.begin();
    #ifdef DEEPSLEEP_ENABLED
        // test if ADM20 data output is active
        #define MAX_WAIT  100
        int i;
        for (i = 0; i < MAX_WAIT; ++i) {
            adm.loop();
            if ( adm.updated() )
                break;
            delay(10);
        }
        if ( i == MAX_WAIT ) {
            // no data received after wakeup -> go to deep sleep
            DEBUG_println(F("direct sleep mode"));
            delay(500);
            ESP.deepSleep(0);
            while (1) { delay(100); }
        }
    #endif

    // init file system
    SPIFFS.begin();
    #ifdef DEBUG
    {
        Dir dir = SPIFFS.openDir("/");
        while (dir.next()) {
            String fileName = dir.fileName();
            size_t fileSize = dir.fileSize();
            DEBUG_SERIAL.printf("FS File: %s, size: %d\n", fileName.c_str(), fileSize);
        }
        DEBUG_println();
    }
    #endif

    String ssid;
    IPAddress ip;

    #ifdef WIFIMANAGER_ENABLED
        //WiFiManager
        #ifndef DEBUG
            wifiManager.setDebugOutput(false);
        #endif
        // wifiManager.resetSettings();

        // load known access points
        load(wifiManager);
        //set config save notify callback
        wifiManager.setSaveConfigCallback(saveConfigCallback);

        wifiManager.setMinimumSignalQuality(20);
        wifiManager.autoConnect(AP_SSID);

        // save known access points
        if ( shouldSaveConfig )
            save(wifiManager);

        // Wait for connection
        while (WiFi.status() != WL_CONNECTED) {
            delay(200);
        }
        ssid = WiFi.SSID();
        ip = WiFi.localIP();
    #else
        // WiFi AP
        WiFi.softAP(AP_SSID);
        ssid = AP_SSID;
        ip = WiFi.softAPIP();
    #endif

    // http server
    httpServer.on("/", HTTP_GET, [](){
        if(!handleFileRead("/index.html"))
            httpServer.send(404, "text/plain", "FileNotFound");
    });
    httpServer.on("/data", HTTP_GET, [](){ httpServer.send(200, "text/json", toDataJson()); });

    //get heap status, analog input value and all GPIO statuses in one json call
    httpServer.on("/status", HTTP_GET, [](){
        String json = "{";
        json += "\"heap\":"+String(ESP.getFreeHeap());
        json += ", \"analog\":"+String(analogRead(A0));
        json += ", \"gpio\":"+String((uint32_t)(((GPI | GPO) & 0xFFFF) | ((GP16I & 0x01) << 16)));
        json += "}";
        httpServer.send(200, "text/json", json);
    });
    httpServer.onNotFound([](){
        if ( !handleFileRead(httpServer.uri()) )
            httpServer.send(404, "text/plain", "FileNotFound");
    });
    #ifdef WIFIMANAGER_ENABLED
        //get heap status, analog input value and all GPIO statuses in one json call
        httpServer.on("/resetwifi", HTTP_GET, [](){
            wifiManager.resetSettings();
            httpServer.send(200, "text/plain", "WiFi settings have been cleared and ESP8266 is going to restart ...");
            // httpServer.send_P(200, F("text/plain"), F("WiFi settings have been cleared and ESP8266 is going to restart ..."));
            delay(500);
            ESP.restart();
        });
    #endif
    #ifdef OTA_UPDATE_ENABLED
        // OTA Update
        httpUpdater.setup(&httpServer);
        httpServer.on("/upload", HTTP_GET, [](){ httpServer.send(200, "text/html", uploadForm); });
        httpServer.on("/upload", HTTP_POST, [](){ httpServer.send(200, "text/html", uploadForm); }, handleFileUpload);
    #endif
    #ifdef UPNP_ENABLED
        // UPnP
        SSDPDevice.setName("ADM20 Digital Multimeter");
        SSDPDevice.setDeviceType("urn:io10-org:device:DigitalMultimeter:1");
        SSDPDevice.setSchemaURL("description.xml");
        SSDPDevice.setSerialNumber(ESP.getChipId());
        SSDPDevice.setURL("/index.html");
        SSDPDevice.setModelName("ADM20-ESP8266");
        httpServer.on("/description.xml", HTTP_GET, []() {
            DEBUG_println(F("send device description"));
            SSDPDevice.schema(httpServer.client());
        });
    #endif
    httpServer.begin();

    // mDNS
    MDNS.begin(AP_SSID);
    MDNS.addService("http", "tcp", 80);

    // web sockets
    webSocket.begin();
    webSocket.onEvent(webSocketEvent);

    // setup finished
    #ifdef DEBUG
        Serial.print(F("Connected to: ")); Serial.println(ssid);
        Serial.print(F("IP address: ")); Serial.println(ip);
        Serial.print(F("Open http://")); Serial.print(AP_SSID); Serial.println(".local");
        Serial.println();
    #endif
    DEBUG_println(F("setup finished"));

    digitalWrite(LED, 1);
}

void loop() {
    static unsigned long msg_time = millis();

    // alive signal (blink every 8 seconds)
    if ( (millis() & 0x1F00) == 0x1000 )
        digitalWrite(LED, 0);
    else
        digitalWrite(LED, 1);

    // handle web server
    httpServer.handleClient();
    webSocket.loop();
    #ifdef UPNP_ENABLED
        SSDPDevice.handleClient();
    #endif

    // update data
    adm.loop();
    if ( adm.updated() ) {
        webSocket.broadcastTXT( toDataJson().c_str() );
        msg_time = millis();
    }


    #ifdef DEEPSLEEP_ENABLED
        // go to deep sleep if no data were received for 15 sec.
        if ( (millis() - msg_time) > 15000L ) {
            // go to deep sleep
            DEBUG_println(F("go to sleep mode"));
            delay(500);
            ESP.deepSleep(0);
            while (1) {
                delay(100);
            }
        }
    #endif
}
