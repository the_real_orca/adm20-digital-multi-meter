/*
BSIDE ADM20 digital multimeter
*/

#ifndef _ADM20_H
#define _ADM20_H

#include <SoftwareSerial.h>
#include <stdint.h>

class ADM20
{
public:
	ADM20(SoftwareSerial &serial) : serial(serial),
		mode(_mode), settings(_settings), value(_value), unit(_unit)
		{}

	void begin();
	void loop();
	bool updated() { return dataHandled; }

	const float &value;
	const String &mode;
	const String &settings;
	const String &unit;

protected:
	static const uint packageSize = 22;
	static const uint8_t header[];
	static const uint headerSize;


	SoftwareSerial &serial;
	uint8_t buf[packageSize];
	uint pos;
	bool dataHandled;

	float _value;
	String _mode;
	String _settings;
	String _unit;

	int sign;

	bool decodePackage();
	float decodeValue(uint8_t data);
};


#endif // _ADM20_H
