#include "ADM20.h"
#include <ESP8266WiFi.h>

const uint8_t ADM20::header[] = {0xAA,0x55,0x52,0x24};
const uint ADM20::headerSize(sizeof(ADM20::header));

void ADM20::begin() {
  serial.begin(2400);
  dataHandled = false;
  pos = 0;

  _value = 0.0;
}

void ADM20::loop() {
  dataHandled = false;
  while ( serial.available() ) {
    auto b = serial.read();

    // compare with header
    if ( pos < headerSize ) {
      if ( b != header[pos] ) {
        // not in sync with header -> wait for next sync
        pos = 0;
        return;
      }
    }

    // add to buffer
    buf[pos] = b;

    pos++;
    if ( pos >= packageSize ) {
      // full package received -> decode
      dataHandled = decodePackage();

      // wait for next package
      pos = 0;
    }
  }
}

static inline bool testMask(const uint8_t data, uint8_t mask) {
  return ((data & mask) == mask);
}

static inline bool testBit(const uint8_t data, uint8_t bit) {
  return testMask(data, (1<<(bit-1)));
}

#define NAME_LEN  16
static void bitsToName(const uint8_t data, const char names[][NAME_LEN], String &str) {
  for (int i = 0; i < 8; i++) {
    if ( testBit(data, i+1) )
    str += names[i];
  }
}

bool ADM20::decodePackage() {
  const uint8_t byteValue = 6;
  const uint8_t bitDecPoint = 8;
  const uint8_t byteMode = 10;
  const uint8_t bitSign = 4;
  const char Modes[8][NAME_LEN] = {"Diode","AC","DC","","","","Connectivity",""};
  const uint8_t byteSettings = 18;
  const char Settings1[8][NAME_LEN] = {"","","","","","AUTO","","REL"};
  const uint8_t bitPercent = 6;
  const uint8_t bitHFE = 7;
  const char Settings2[8][NAME_LEN] = {"","MAX","-","MIN","","","",""};
  const uint8_t byteUnit = 20;
  const char Units1[8][NAME_LEN] = {"&deg;C","&deg;F","","","","&micro;","n","F"};
  const char Units2[8][NAME_LEN] = {"&micro;","m","A","V","M","k","&Omega;","Hz"};


  // decode value
  sign = 1;
  float val = decodeValue(buf[byteValue]) + decodeValue(buf[byteValue+1])*10L + decodeValue(buf[byteValue+2])*100L + decodeValue(buf[byteValue+3])*1000L;
  if ( testBit(buf[byteValue], bitDecPoint) )
  val = val / 10;
  else if ( testBit(buf[byteValue+1], bitDecPoint) )
  val = val / 100;
  else if ( testBit(buf[byteValue+2], bitDecPoint) )
  val = val / 1000;
  else if ( testBit(buf[byteValue+3], bitDecPoint) )
  val = val / 10000;
  if ( testBit(buf[byteMode], bitSign) )
  sign = -1;
  _value = sign * val;

  // decode unit
  _unit = "";
  if ( testBit(buf[byteSettings+1], bitPercent) )
    _unit = "%";
  bitsToName(buf[byteUnit], Units1, _unit);
  bitsToName(buf[byteUnit+1], Units2, _unit);



  // set mode
  _mode = "";
  if ( testBit(buf[byteSettings+1], bitHFE) )
    _mode = "hFE";
  bitsToName(buf[byteMode], Modes, _mode);

  // set settings
  _settings = "";
  bitsToName(buf[byteSettings], Settings1, _settings);
  bitsToName(buf[byteSettings+1], Settings2, _settings);

  //  Serial.print(" "); Serial.print(_mode); Serial.print(" ");Serial.print(_settings); Serial.print(" "); Serial.print(_value); Serial.println(_unit);

  return true;
}

float ADM20::decodeValue(uint8_t data) {
  float val = 0;

  switch (data & 0x7F) {  // ignore decimal point
    case 0x00:
    case 0x5F:
    val = 0; break;
    case 0x06:
    val = 1; break;
    case 0x6B:
    val = 2; break;
    case 0x2F:
    val = 3; break;
    case 0x36:
    val = 4; break;
    case 0x3D:
    val = 5; break;
    case 0x7D:
    val = 6; break;
    case 0x07:
    val = 7; break;
    case 0x7F:
    val = 8; break;
    case 0x3F:
    val = 9; break;
    case 0x20:
    sign = -1; break;
    case 0x58:
    val = NAN; break;
    case 0x79:
    val = NAN;
    // TODO error handling
    break;
  }
  return val;
}
