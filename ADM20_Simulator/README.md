ADM20 Simulator
===============

##**!-!-! DISCONTINUED !-!-!**


The idea of the ADM20 simulator was to help with debugging the protocol and send generated data packages to the original DMM Data Logger software. However, the DMM SW uses flow control (RTS, DTR) to communicate with the multi meter. This conflicts with the NodeMCU or other Arduino based modules, since Arduino uses the flow control lines to switch the modules into programming mode. So the NodeMCU starts to reset every time the DMM SW connects to the simulator. Even if it is able to solve this with a HW hack, there is no flow control available from the SW side of an Arduino program. 

So I decided to discontinue this sub project and decode the ADM20 protocol using the traditional sniffing approach. See the [main project page](https://bitbucket.org/the_real_orca/adm20-digital-multi-meter/overview) for the **protocol description**.
