// WiFi
#include <ESP8266WiFi.h>
#include <WiFiClient.h>

// WiFi Manager
// https://github.com/tzapu/WiFiManager
#include <WiFiManager.h>         

// http server
#include <ESP8266WebServer.h>
ESP8266WebServer server(80);

// web sockets
// https://github.com/Links2004/arduinoWebSockets
#include <WebSocketsServer.h>
WebSocketsServer webSocket = WebSocketsServer(8080);

#define STR_LEN  32
struct section_t {
  uint16_t pos;
  char  label[STR_LEN];
};

#define BUF_SIZE	22
uint8_t buf[BUF_SIZE] = {0xAA, 0x55, 0x52, 0x24, 0x01, 0x10, 0x5F, 0x5F, 0xDF, 0x5F, 0x24, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x01, 0x00, 0x08};
const section_t sections[] = {{6, "Header"}, {10, "Value"}, {11, "AC/DC"}, {19, "Analog Bar ???"}, {21, "???"}, {22, "Mode"}};

// JSON
// https://bblanchon.github.io/ArduinoJson/
#include <ArduinoJson.h>
DynamicJsonBuffer jsonBuffer(BUF_SIZE*10);
String html;

#define LED 2

void handleRoot() {

	
	// header
	html = F("<html>");
	html += 
			#include "header.html.h"
			;
	html += F("<body><h1>ADM20 Simulator</h1>");

  uint8_t sec = 0;
  html += F("<div class='section'><h2>"); html += String(sections[sec].label); html += F("</h2>");
	for (int i=0; i < BUF_SIZE; i++) {
    if ( i == sections[sec].pos ) {
      sec++;
      html += F("</div><div class='section'><h2>"); html += String(sections[sec].label); html += F("</h2>");
    }
		html += F("<div class='digit'><label>"); html += String(i+1); html += String(F("</label>\n"));
    html += F("<input type='text' class='hex' value='00' oninput='handleValueInput(this)'><br>\n<input type='checkbox' class='bin' onchange='handleBitCheckbox(this,0x80)'><input type='checkbox' class='bin' onchange='handleBitCheckbox(this,0x40)'><input type='checkbox' class='bin' onchange='handleBitCheckbox(this,0x20)'><input type='checkbox' class='bin' onchange='handleBitCheckbox(this,0x10)'> <input type='checkbox' class='bin' onchange='handleBitCheckbox(this,0x8)'><input type='checkbox' class='bin' onchange='handleBitCheckbox(this,0x4)'><input type='checkbox' class='bin' onchange='handleBitCheckbox(this,0x2)'><input type='checkbox' class='bin' onchange='handleBitCheckbox(this,0x1)'></div>\n");
	}
  html += F("</div></body></html>");
	
	server.send(200, "text/html", html);
}


void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {

    switch(type) {
        case WStype_DISCONNECTED:
            break;
        case WStype_CONNECTED:
            break;             
        case WStype_TEXT:
            jsonBuffer.clear();
      			JsonArray& array = jsonBuffer.parseArray(payload);
      			int size = array.size();
      			if ( size > BUF_SIZE )
      				size = BUF_SIZE;
      			for (int i=0; i<size; i++) {
      				buf[i] = array[i];
      			}

            jsonBuffer.clear();
            JsonArray& json = jsonBuffer.createArray();
            for (int i=0; i<BUF_SIZE; i++) {
              json.add(buf[i]);
            }
            String data;
            json.printTo(data);
            webSocket.sendTXT(num, data.c_str());
            break;
    }

}

void setup() {
  pinMode(LED, OUTPUT);
  digitalWrite(LED, 1);
  delay(500);
  digitalWrite(LED, 0);

  
  Serial.begin(2400, SERIAL_8N1);
  while(!Serial) {}
  Serial.println();

  //WiFiManager
  WiFiManager wifiManager;
  wifiManager.setDebugOutput(false);
  wifiManager.autoConnect("ADM20");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }

  
  // http server
  server.on("/", handleRoot);
  server.onNotFound( [](){server.send(404, "text/plain", "File Not Found\n\n");} );
  server.begin();
  html.reserve(20480);
  
  // web sockets
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);  
  
  Serial.print("Connected to "); Serial.println(WiFi.SSID());
  Serial.print("IP address: "); Serial.println(WiFi.localIP());
}

void loop(void) {
  static long timestamp = 0;
  const static int dt = 150;
  server.handleClient();
  webSocket.loop();

        
  // simulate ADM20 serial output
  if (( millis() - timestamp ) > dt ) 
  {
    digitalWrite(LED, 0);
	  timestamp = millis();
    Serial.write(buf, BUF_SIZE);
    delay(10);
    digitalWrite(LED, 1);
  }
  
}
