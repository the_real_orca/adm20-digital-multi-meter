R"(
<head>
<script>
var wsURL = 'ws://' + window.location.hostname + ':8080/';
var ws;

window.onload = function () {
  ws = new WebSocket(wsURL);

  ws.onmessage = function (e) {
    var data = JSON.parse(e.data);
    console.log(data);
    var digits = document.getElementsByClassName("digit");
    for (var d=0; d<digits.length && d<data.length; d++) {
      update(digits[d], data[d], true);
    }  
  };

  ws.onopen = function () {
    // trigger data response
    ws.send("[]");
  }
};

function sendData() {
	var data = [];
	var digits = document.getElementsByClassName("digit");
	for (var d=0; d<digits.length; d++) {
		data.push( getValue(digits[d], true) );
	}
	ws.send(JSON.stringify(data));
}

function toHex(v) {	return ('0'+v.toString(16)).slice(-2).toUpperCase();}
function toBin(v) {    return ('00000000'+v.toString(2)).slice(-8);}
function update(x, v, isParent = false) {
  if ( !isParent )
    x = x.parentElement;
	var input = x.getElementsByClassName("hex")[0];
	input.value = toHex(v);
	var list = x.getElementsByClassName("bin");
	for (var i=0, m=0x80; i<8; i++, m=m>>1) {
		list[i].checked = ( v & m );
	}
}
function getValue(x, isParent = false) {
	if ( !isParent )
		x = x.parentElement;
	var list = x.getElementsByClassName("hex");
	return parseInt(list[0].value,16);
}

function handleValueInput(el) { update(el, el.value); sendData();}
function handleBitCheckbox(el, m) { update(el, el.checked ? (getValue(el) | m) : (getValue(el) & ~m) ); sendData();}


</script> 
<style>
  h2 {
    margin: 1ex;
  }
  .section {
    border: gray 2px outset;
    margin: 0.5ex;
    padding: 0.5ex;
  }
	.digit {
		text-align:center;
		display: inline-block;
		padding: 0px;
    margin: 0.5ex;
	}
	.digit > label {
		display: block;
		font-family: Arial, Helvetica, sans-serif;
		font-weight: bold;
	}
	.digit > label::before {
		content: "#"
	}	
	input.hex {
		text-align:center;
		font-size: 250%;
		width: 3.5ex;
	}
	.bin {
	  transform: scale(0.75);
	  padding: 0px;
	  margin: 0px;
	}	
</style>
</head>
)"
