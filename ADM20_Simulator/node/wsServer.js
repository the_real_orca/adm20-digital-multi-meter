'use strict';

// https://vimeo.com/212379936

const http = require('http');

const express = require('express');
const app = express();

const ws = require('ws');


app.get('/', (req, res) =>{
	console.log("request on '/'");
	res.send("Hello world");
});

const server = http.createServer(app);

const wsServer = new ws.Server({server});

wsServer.on('connection', socket => {
	console.log("connected");
	socket.on('message', message => {
		console.log("message: ", message);
		const data = JSON.parse(message);
		socket.send(JSON.stringify({message: data}));
	});
});



server.listen(8080, () => {
	console.log("Server is running on port: 8080 ...");
});