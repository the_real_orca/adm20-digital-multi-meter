WEMOS D1 mini Modification
==========================

The on board voltage regulator supplies both, the ESP8266 and the CH340G (serial to USB IC).
To reduce power during deep sleep we want to eliminate the voltage regulator and activate it only when the CH340G IC is used for upload or serial communication.

This can be done by separating the power supply of the ESP8266 and the CH340G.
The ESP8266 will be powered by the 3v3 pin. (Make sure that you have a stable power supply for the 3.3V.)
The CH340G will be powered by USB. As soon as the USB cable is connected, the on board voltage regulator will generate the 3.3V out of the 5V from the USB connection, and the serial to USB interface is available.
 
## Modification

- Cut the 3v3 trace between the CH340G IC and D7
(Take care that you cut the 3v3 trace immediately after the IC. The second trace right of it and the 3v3 branch between the 3v3 pin and the capacitor next to D6 must not be cut.)

- Cut the 3v3 trace between the lower left pin of the CH340G and the middle resistor between D3 and D4.

- Add a jumper wire between the resistor next to D3 and the capacitor next to D6.


**Testing:**

- You should measure a resistance of 10kOhm between pin D3 and the 3v3 pin.

- There should be no connection between the 3v3 pin and the  lower left pin of the CH340G.


![WEMOS D1 mini PCB](https://bytebucket.org/the_real_orca/adm20-digital-multi-meter/raw/967069caf7120d7b59ca4e7ceaf5ff24a1f67179/images/WIMOS%20D1%20mini%20mod.jpg)
