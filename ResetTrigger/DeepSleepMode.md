Deep Sleep Mode
===============

To reduce the power of the ESP8266 it can be set into deep sleep when not in use. When the USB (now WiFi) output is activated on the ADM20, it should wake-up the ESP8266 automatically. So we need to add some extra components to get this working.

## Reset on USB Activation

To reactivate the ESP8266 after deep sleep, it needs to be retested.
When USB output is enabled on the ADM20, it sends a burst of data (packages) every 200ms. 

This cirtuit pulls the reset line down, every time the USB (->WiFi) output is enabled.

![Reset Schematics](https://bytebucket.org/the_real_orca/adm20-digital-multi-meter/raw/801cb08bda0dce01a49b1fd702bfe590c1716840/ResetTrigger/ResetTrigger.png)

The Tx Signal is the same that is used to decode the data send from the ADM20 (between Q2 and R25 on the [ADM20 PCB](https://bytebucket.org/the_real_orca/adm20-digital-multi-meter/raw/34a3fd55fbc0d1a5ed39b6a63264d0d4d9e7df4e/images/ADM20_PCB.jpg)).

## Power Consumption

Measurements for WEMOS D1 mini:

|                    |  5V    |  3v3   | 3v3 with modification |
|--------------------|-------:|-------:|-------:|
|**normal operation**|   79mA |   78mA |   76mA |
|**deep sleep**      | 0.25mA | 0.18mA | 0.02mA |

see: [WEMOS D1 mini Modification](WEMOS%20D1%20mini%20Modification)
